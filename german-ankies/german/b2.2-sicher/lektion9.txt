# KB s 124 Meine Motivation für ein Masterstudium an der Universität Fribourg

der Studiengang ; Studienplatz in einem Masterstudiengang an Ihrer Hochschule bewerben. ; Academic discipline
bereits ; Im vergangenen Jahr habe ich bereits die Universität Fribourg besucht ; already
die Aufgeschlossenheit ; Ich war beeindruckt von der freundlichen Atmosphäre und der Aufgeschlossenheit der Lehrkräfte. ; open-minded, openess
aufschließen ; ; to unlock
die Lehrkraft ; ; the teacher
verfügen + über ; Nach mehreren Sprachkursen verfüge ich über sehr gute Deutschkenntnisse. ; to have the ability, to have the qualification.
überdies ; Überdies werde ich ab März bis Ende Mai dieses Jahrs ein Praktikum an einer Grundschule in Nordrhein-Westfalen absolvieren ; moreover
absolvieren ; ; to pass (a test/ a course)
sich mit etw vertraut machen ; Hier würde ich mich gern mit der neuesten Forschung vertrauen machen ; to become familiar with



