aufgehen ; In der BVG-App geht ihr Fahrplan auf. ; to open, to rise, to work out
eingeben ; Dann gebt ihr eine Station ein ; to enter, input

sich vordrängeln ; Es ist typisch Deutsch, dass sich immer alle vordrängeln wollen ; cut in line, לקצר את התור
umkippen ; Michael hat seinen Kaffee umgekippt. ; to overturn, to topple
der Lappen ; Michael geht in die Küche und holt einen Lappen. ; rag, סמרטוט
sauber wischen ; Gleich kommt er wieder und wischt seinen Schreibtisch sauber. ; to wipe

