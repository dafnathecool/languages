#!/bin/python

import sys
import re
import glob

if len(sys.argv) != 1 and len(sys.argv) != 2:
    print ("usage:\n%s <output-file> (or stdout if not given)" % sys.argv[0])
    print ("output file format: '<german-word> ; <context> ; <english>")
    sys.exit();

if len(sys.argv) == 2:
    outfile=sys.argv[1]
    outf = open(outfile, "w")
else:
    outf = sys.stdout

outf.write('tags: learn-german\n\n')
for filepath in glob.glob('**/*.txt', recursive=True):
    if 'translations.txt' in filepath:
        continue
    inf = open(filepath, "r")

    for line in inf:
        line = line.strip()
        if not line or line.startswith("#"):
            continue
        if re.search("^[A-Z]", line):
            sys.stderr.write("warning: noun without an article:\n")
            sys.stderr.write(line)
            sys.stderr.write('\n')
            continue
        if re.search("^(Der|Die|Das) ", line):
            sys.stderr.write("warning: article in capital letter:\n")
            sys.stderr.write(line)
            sys.stderr.write('\n')
            line = "d" + line[1:]

        fields = line.split(";")
        if len(fields) == 3:
            outf.write(fields[0].strip())
            outf.write(' ; ')
            outf.write(fields[1].strip())
            outf.write(' ; ')
            outf.write(fields[2].strip())
            outf.write('\n')
        else:
            sys.stderr.write("bad line:")
            sys.stderr.write(line)
            sys.stderr.write(line[0])
            sys.stderr.write('\n')
    inf.close()

outf.close()


