﻿tags: english-learning
poaching ; unless their babies are under threat from poachers. ; illegal hunting
maul ; The intent is to keep the infant safe, not to maul a human. ; to savage, to cause serious physical wounds
canine tooth ; they will also bite with a fierce set of canines ; ניב
Oblivious ; Oblivious to their own pioneering spirit. ; unaware, lacking awareness, unconscious of
Primate ; the most rigid of social organizations amongst any of the primates on earth. ; פרימטים, סדרת הקופים במחלקת היונקים
Thistle ; Thistles and bitter branches ; קוץ
gouge ; while using its feet to gouge your eyes out while the other hands.. ; לחרוט
funnel ; It could be compared to a funnel that you fill with water until you get the answer that you want to hear. ; משפך
averse ; and he soon became rich entrepreneur who was not averse to splendor and a patron for many artists. ; having a strong dislike or opposition to something.
zest ; August Borsig was said to be a strict but just boss with a zest of action ; enjoyment, enthusiasm
to weather ; he was able to weather the economic crisis of 1848-1852 with little damage. ; to endure, to resist
inaugurate ; The works was inaugurated in 1898. ; initiate something with an official ceremony
Arbitration ; The process to determine which of the masters on the bus can use it when more master need to use the bus. ; בוררות
to dawn ; The day of freedom and of bread dawns ; to begin, to brighten with daylight
auspiciousness ; In the western world it was a symbol of auspiciousness ; success
emblem ; emblem became a feature of Nazi symbols as an emblem of the Aryan race ; symbol
wacky ; Gnome Desktop's configuration settings are all wacky ; unusual, eccentric
stagger ; they must be written to the sensor in an appropriately staggered manner ; walk or move unsteadily is if about to fall
hiatus ; After a long hiatus, Ispell 3.4 primarily offers bug fixes and increased portability ; A gap in a series making it incomplete
deed ; and often use a command line spelling checker called aspell to do the deed ; an action, an act, something that is done
peppy ; Ispell is still a peppy application that you can use ; energetic, cheerful,
poignant ; This poignant narrative is followed by a variety of tales representing the diversity of the time and including satires, explorations of private obsessions and experiments in form and language. ; עז, חריף
cringe ; I cringe at the overused jokes in this thread ; להרתע, להתכווץ
eminence ; The war became less about religion and more of a continuation of the France-Habsburg rivalry for European political pre-eminence and Habsburg attempt to rebuild the imperial authority ; noble, איצילי, רם מעמד
instigate ; The war was instigated by the election of Ferdinand II as Holy Roman Emperor, a staunch Catholic ; להמריץ, לדרבן
staunch ; The war was instigated by the election of Ferdinand II as Holy Roman Emperor, a staunch Catholic ; איתן, נאמן, מסור
ebb ; Initial Swedish successes brought them deep into Catholic territory in southern Germany, but Swedish fortunes ebbed after Gustavus Adolphus was killed at the Battle of Lützen (1632). ; שפל, נסיגה
curtail ; The rise of Bourbon France, the curtailing of Habsburg ambition, and the ascendancy of Sweden as a Great Power created a new balance of power ; to limit, to restrict
truce ;  The Dutch revolted against Spanish domination during the 1560s, leading to a protracted war of independence that led to a truce only in 1609. ; הפסקת אש
plunder ; Soldiers plundering a farm by Sebastian Vrancx, 1620 ; לשדוד, לבזוז
famine ; The war ranks with the worst famines and plagues as the greatest medical catastrophe in modern European history. ; רעב, מחסור חמור
maraud ; Villages were especially easy prey to the marauding armies. ; מסע בזיזה ורצח
exacerbate ;  but may have done no more than seriously exacerbate changes that had begun earlier. ; to make a problem worse
calamity ; Residents of areas that had been devastated not only by the conflict but also by the numerous crop failures, famines, and epidemics that accompanied it were quick to attribute these calamities to supernatural causes.  ; An event resulting in great loss.
heed ; The chip is 'deaf' and pays no heed to changes in the state of its other input pins ; careful attention
rouge ; This was used to prevent rogue applications from becoming master and/or failing to release it ; בלתי נשלט, מסוכן, נוכל
air horn ; When you saw it coming you'd blow an air horn to request that it pull over ; מגאפון
regency ; The period from the start of the regencies of Anna Amalia ; שלטון מקומי, שליט זמני
prudence ; During Karl August's minority she administered the affairs of the duchy with notable prudence, ; שיקול דעת
foster ; The first emphasis was fostering music. ;  parental care to children, אימוץ
curation ; As early as the 19th century, the curation of Weimar and its heritage started. ; אוצרות (במוזיאון(
incarcerate ; some 240,000 people were incarcerated in the camp by the Nazi regime, ; לכלוא
homage ; The town adopted the name Illiers-Combray in homage. ; מחווה אומנותית
steeple ; the country town overlooked by a church steeple where Proust spent time as a child and which he described as "Combray" ; מגדל של כנסיה
spire ; ; שפיץ בקצה מגדל של כנסיה, צריח מחודד
contrived ; A contrived example: ; Obviously planned or calculated, not spontaneous or natural, מתוכנן, מלאכותי
viable ; but power state of the system as a whole, is a very viable architecture. ; Able to live on its own, able to be done, possible
amend ; Please, amend your timesheets. Your Project Manager can help if needed. ; to make better, to improve
quiesce ; If the device is still present, it should quiesce the device and place it into a supported low-power state. ; To become quiet or calm, become silent.
in anticipation of ; in order to be able to inhibit a device in anticipation without needing to open it first. ; expecting that (something will happen or that someone will arrive)
alleviate ; Call oom_kill, which kills a process to alleviate an OOM condition ; To make (pain, for example) less intense or more bearable:
terse ;  brief, concise ; Output a terse help document to the console
thaw ; Forcibly "Just thaw it" – filesystems frozen by the FIFREEZE ioctl. ; to melt
far-fetched ; Right, I can see now that the suggestion to change names is too far fetched. ; Not likely, difficult to believe.
rudimentary ; control codes for rudimentary line editing and so on ; minimal, basic
lurk ; But as we shall see, the legacy from the old cast-iron beasts is still lurking beneath the surface. ; To remain concealed in order to ambush.
chauffeur ; The RAF has been held responsible for thirty-four deaths, including many secondary targets, such as chauffeurs and bodyguards, ; נהג מכונית
premises ; members of the later Revolutionary Cells took part in bombings of the premises of ITT ; (plural only) land, and all the built structures on it, especially when considered as a single place.
raid ; Revolutionary Cells member Hans-Joachim Kl	 	  ein took part in the December 1975 raid  ;  פשיטה (צבאית(
detonate ; the Revolutionary Cells claimed responsibility for many bombs detonated shortly before he arrived. ; to explode
pamphlet ;  In a pamphlet published in December 1991, the Revolutionary Cells attempted a critical review of their so-called anti-imperialist and anti-Zionist campaign ; A small booklet of printed informational matter, often unbound, having only a paper cover.
albeit ; mid-year self-evaluation (albeit in August): Engineers have performed a light mid-year self-evaluation:; although, despite
allude ; As alluded to above, the 2020 Appraisal form has been slightly revamped and will be available in Zoho People. ; לרמוז, כמשתמע
revamp ; As alluded to above, the 2020 Appraisal form has been slightly revamped and will be available in Zoho People.  ; to renovate, to improve
shun ; User-space mode setting would require superuser privileges for direct hardware access, so kernel-based mode setting shuns such requirement for the user-space graphics server. ; to presistently avoid
proliferation ; A more pressing issue was the proliferation of graphical applications bypassing the X Server and the emergence of other graphics stack alternatives to X, ; התרבות
waive ; Many credit unions are willing to waive the fee when asked.  ; To relinquish (a right etc.) to give up claim to
wedded ; Because the specification is not wedded to a particular algorithm, if one is found to be weak at some point in the future, then instead of the specification having to be rewritten, just that algorithm can be removed from the list of approved algorithms. ; join in marriage
equate ; People often equate security solely with secrecy: the inability of an attacker to decode a secret message.  ; To consider equal or equivalent.
pertinent ; Processes and decisions pertinent to business are greatly dependent on integrity. ; important with regard to (a subject or matter); relevant
vex ; One problem that vexed the developers for a long time was how to integrate multiple hash algorithms ; To trouble aggressively, to harass. , To annoy, irritate
savvy ; For the more computer science savvy readers, matching these CRTCs and Encoders is an NP-complete, and is an example of the Boolean satisfiability problem.  ; Shrewd, well-informed and perceptive.
contention ; This is kind of a big deal, and is the cause of much contention between the open source drivers and Nvidia's proprietary driver. ; Argument, contest, debate, strife, struggle.
to thaw ; you can freeze the pizza and later thaw it ; להפשיר
looming ; The Climate Crisis is looming large, and it’s a much greater danger, for which there will be no vaccination available, ; הֵגִיחַ, הוֹפִיעַ (בעיקר באופן מעורפל ומעורר אימה)
retention ; I actually read that typing (or even more so, writing) things helps with retention ; היכולת לשמור משהו בזיכרון
courtesy ;  the power flow can even reverse in real time courtesy of a communication channel carried on the Type-C Configuration Channel (CC) pins. ; a polite gusture
pesky ; ideally should be reviewed by someone knowing more how the ISP and  PHYs work on those pesky rk3399 and rk3288 ; annoying, troublesome, irritating
errand ; can we do it 10:30 argentina/2:30 spain instead? I have to go for an errand ; A journey undertaken to accomplish some task.
addendum ; InterChip USB is an addendum to the USB Implementer Forum's USB 2.0 specification. ; Something to be added; especially text added as an appendix or supplement to a document.
lumpsum ; The tax law provides a general lumpsum for the general expenses ; paid at one time
stencil ; By placing the stencil on top of another object, then spraying paint through the hole, you can very quickly produce stenciled patterns in many different colors! ; שבלונה
indemnity ; In 2020 we signed a new MSA with Google, however it doesn’t include indemnitiy terms for Open Source Software.  ; An obligation or duty upon an individual to incur the losses of another.
discretion ; the time spent on R&D was purely at the discretion (and subject to the whims) of the domain lead. ; The freedom to make one's own judgements.
prudent ; It would be prudent to also provide an implementation of @enable if you are expecting driver calls into &drm_bridge_chain_enable. ; Practically wise, judicious, shrewd. זהיר, שקול
vigilant ; never place device on the ground or beside you on a counter and be vigilant at all times. ; Watchful, especially for danger or disorder, alert
vacancy ;  Internal vacancies will be displayed on the company website and distributed via email. ; An unoccupied position or job.
discretion ; have decided to exercise our discretion to pay you a one-time incentive payment ; שיקול דעת , איפוק , ריסון
incentive ; have decided to exercise our discretion to pay you a one-time incentive payment ; A bonus or reward, often monetary, to work harder.
grit ; I promise to return to the gritty detail in a later post. ; חצץ דק
dourght ; During a drought, there is hardly any water available. ; בצורת
eloquent ; indeed, but as the Rolling Stones eloquently put it, you can't always get what you waaaannnttt ; articulated, effective in expressing meaning by speech
indispensable ; Names allow linking to elements created previously in the description, and are indispensable to use elements with multiple output pads, ; Absolutely necessary or requisite; that one cannot do without
spur ; ; עורר, דירבן, המריץ
stangate ; ; דרך במקום, קפא על השמרים,
Savvy ; ; מיודע , מומחה
abreast ; ; יחד, זה לצד זה, בשורה אחת
wane ; the pace of hiring typically starts to wane ;  פָּחַת, הִתְמַעֵט, הִצְטַמְצֵם; דָּעַךְ, גָּוַע; הִתְפּוֹגֵג 
protracted ; ; ממושך, ארוך
malaise ; ;  תחושה של חוסר-נוחות, בחילה קלה או דיכאון
falter ; ; קִרְטֵעַ, הִתְנוֹדֵד, כָּשַׁל; הִסֵּס, הִתְחַבֵּט; גִּמְגֵּם
lay off ;; לפטר
accrued ; ; נצבר, הצטבר, גדל, צמח
par ; ; שווי, ערך
distill ; ; זיקק, טפטף, הזליף
tally ; ; חשבון, סיכום, תוית, תג
cognizant ; ; מודע
surmise ; ; ניחש, שיער
redemption ; ; פדיון

just society ; ; חברה צודקת
plebs ;; המון אנשים, ציבור
staple ;; שידך, הכליב, מרכיב מרכזי

steer away from ; ; לכוון הרחק מ-
futile ; ; עקר, חסר ערך
subsidiary ; ; מסייע, תומך, משני
curate ; ; עצר בתערוכה, שימש כאוצר בתערוכה
persuade ; ; שכנע
manipulate money ostensibly to “grow,” or “stabilize,”  ; כביכול, לכאורה
purport ; ; התיימר
linchpin ; ; רכיב מרכזי
squander ; ; ביזבז, פיזר
History is wrought with economic distortions ; ; מעוצב, מעובד, יצוק, מחושל
debase ; ; מהל, קלקל, זייף
folly ;; טיפשות
discern ;; הבחין, הבדיל, תפס
decree ;; צו, פסק דין
wary ; ; זהיר
begrudgingly ; ; תוך קינאה, נטירת טינה

plunge ; During stock market crashes Gold may plunge ; צָנַח, נָפַל, הִדַּרְדֵּר, צָלַל, טָבַל; קפץ למים; ירד במדרון תלול 
sell off ; ; מכירת חיסול
GFC (global financial crisis) ; ; משבר 2008






