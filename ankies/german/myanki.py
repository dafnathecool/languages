#!/usr/bin/python3

import argparse
from numpy import random
import numpy as np

def one_file(filepath):
    inf = open(filepath, "r")
    lines = [line for line in inf.readlines()]
    idx = np.array(list(range(0, len(lines))))
    if args.rand:
        random.shuffle(idx)
    num = min(len(idx),args.num) if args.num else len(idx)

    for n in range(0, num):
        line = lines[idx[n]].strip()
        if not line or line.startswith("#"):
            print(line)
            continue
        fields = line.split(";")
        if len(fields) >= 3:
            front = fields[0].strip()
            back = fields[1].strip()
            context = fields[2].strip()
            print("== %d ==" % (n + 1))
            print(front)
            print(context)
            text = input()  # Python 3
            print(back)
            input()  # Python 3
    inf.close()

parser = argparse.ArgumentParser("good luck")

parser.add_argument("-i", "--in-file", help="the input file")
parser.add_argument("-r", "--rand", help="randomize the words", action='store_true')
parser.add_argument("-n", "--num", type=int, help="limit the number of words to 'num'")

args=parser.parse_args()

if args.in_file:
    one_file(args.in_file)


