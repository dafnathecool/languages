* `der Unterschied` ~~die Unterschied~~

* Der Film Mivtza Sawta (übersetzt: Operation Großmutter) ist im Jahr 1999 ~~erscheinen~~ (`erschienen`)

* Am Ende schaffen sie ~~das~~ `es` irgendwie,

* Durch ~~die~~ `den` Unterschied zwischen ~~die~~ `der` Ernsthaftigkeit, die man von einer Beerdigungszeremonie erwartet, und ~~die~~ `der` improvisatorischen Art und Weise (zwischen + Dativ)

* Der Film macht sich auch sehr ~~Lustig~~ `lustig` über das Leben im Kibbuz. 

* Dort ~~werbt~~ der große Bruder eine ehrenamtliche Frau ~~um~~ : `umwerben` is UNTRENNBAR -> Dort umwerbt der große Bruder eine Frau.

* Er schwimmt Unterwasser, bis er ~~zu~~ ~~der~~ `die` Frau erreicht.

* Dies hat mir ~~besonderes~~ `besonders` gefallen.

* Im ~~laufe~~ `Laufe` der Zeit

* ~~eins~~unddreißig -> **ein**unddreißig

* Das ist ~~herausforderlich~~ `herausfordernd`

