#!/usr/bin/python3

import sys
import re
import glob
import os
import argparse

def test_article(lexeme, filepath):
    if len(lexeme.split()) == 1 and re.search("^[A-Z]", lexeme):
        sys.stderr.write("warning: noun without an article: %s\n" % os.path.basename(filepath))
        sys.stderr.write(lexeme)
        sys.stderr.write('\n\n')
    if re.search("^(Der|Die|Das) ", lexeme):
        sys.stderr.write("warning: article in capital letter: %s\n" % os.path.basename(filepath))
        sys.stderr.write(lexeme)
        sys.stderr.write('\n\n')

def write_one_file(filepath, outf):
    inf = open(filepath, "r")

    is_first_line = True
    tag = None
    for line in inf:
        line = line.strip()
        if not line or line.startswith("#"):
            continue
        if is_first_line and line.find(";") < 0:
            tag = line.replace(" ","-")
            continue
        is_first_line = False
        fields = line.split(";")
        if len(fields) == 3:
            test_article(fields[0].strip(), filepath)
            outf.write(fields[0].strip()) # Front
            outf.write(' ; ')
            outf.write(fields[2].strip()) # Back
            outf.write(' ; ')
            outf.write(fields[1].strip()) # Context
            if tag:
                outf.write(' ; ')
                outf.write(tag) # Tag
            outf.write('\n')
        else:
            sys.stderr.write("bad line in file %s:\n" % os.path.basename(filepath))
            sys.stderr.write(line)
            sys.stderr.write(line[0])
            sys.stderr.write('\n\n')
    inf.close()

parser = argparse.ArgumentParser("good luck")

parser.add_argument("-o", "--out-file", help="the output file, defualt it stdout")
parser.add_argument("-i", "--in-file", help="the input file, default is all files that match '**/*.txt'")
parser.add_argument("-t", "--tag", help="the tag to add")

args=parser.parse_args()

if args.out_file:
    outf = open(args.out_file, "w")
else:
    outf = sys.stdout

if args.tag:
    outf.write("tags: {tag}\n\n".format(tag=args.tag))
if args.in_file:
    print(args.in_file)
    # for filepath in glob.glob(args.in_file, recursive=True):
    # for filepath in glob.glob('*.*.202*.txt'):
    for filepath in glob.glob(args.in_file):
        if filepath == args.out_file:
            continue
        print("# === %s ===" % filepath)
        write_one_file(filepath, outf)
else:
    for filepath in glob.glob('**/*.txt', recursive=True):
        if filepath == args.out_file:
            continue
        write_one_file(filepath, outf)

outf.close()


