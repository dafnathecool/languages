der Fahrstreifen ; Bei zwei Fahrstreifen für eine Richtung das Reißverschlussverfahren anwenden  ; lane / lanes
der Reißverschluss ; Bei zwei Fahrstreifen für eine Richtung das Reißverschlussverfahren anwenden  ; zipper, ריץ רץ
das Reißverschlussverfahren ; Bei zwei Fahrstreifen für eine Richtung das Reißverschlussverfahren anwenden  ; merging lanes
nach sich ziehen ;  Welche Folgen kann die Missachtung von Müdigkeitsanzeichen nach sich ziehen?  ; to entail, to bring
gewähren ; - an der Sichtlinie anhalten und Vorfahrt gewähren ; grant, להעניק
der Bahnübergang ;  Ich darf das Mofa  nicht vor dem Bahnübergang überholen  ; חציית רכבת
die Beanspruchung ;  Überbeanspruchung der Bremsen  ; use, stress
betätigen ; Feststellbremse betätigen ; to operate
der Rückwärtsgang ; Ersten Gang oder Rückwärtsgang einlegen ; reverse gear
die Leerlaufstellung ;  Schalthebel in Leerlaufstellung bringen  ; neutral position
gähnen ; Gähnen ; to yawn, לפהק
abschleppen ; Sie wollen mit Ihrem Pkw einen anderen abschleppen. Was müssen Sie wissen? ; to tow
die Faustformel ; Nach welcher Faustformel kann man aus der Geschwindigkeit den Weg in Metern ermitteln, den ein Fahrzeug in einer Sekunde zurücklegt?  ; rule of thumb
die Straßeneinmündung ;  An Straßenkreuzungen und -einmündungen ; T-junction
verkehrsberuhigten ; Am Ende eines verkehrsberuhigten Bereiches ; רחוב משולב
die Steigung ; In einer schmalen Straße wollen Sie in einer Steigung Radfahrer überholen.  ; slope

