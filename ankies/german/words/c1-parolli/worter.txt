wiederstehen ; ich kann den Kuchen nicht wiederstehen ; ~verkneifen
der Widerstands ; ; resistance
die Beschwerde ; ; complain
der Verstand ; ich habe wenigen Verstand wenn ich trinke ; חשיבה רציונלית
nachvorziehen ; ; to relate, to emphesize
das Verständnis ; ich zeige Verständnis für dein Problem. ; הבנה
uns erhalten bleiben ; Die Wörter bleiben uns erhalten ; remain among us
die Entsprechung ; weil es in anderen Sprachen keine Entsprechung  gibt ; equivalent
in jünger Zeit ; in jünger Zeit entwickelt der Schwede 'Fingerspitzengefühl' ; recently
geraten in ; und Russen geraten neuerdings in Zeitnot ; to get into
sich hervortun ; Sie tun sich besonderes hervor in Sachen ; distinguish oneself
das Oberlicht ; als sie Oberlichter sahen ; Dachfenster
einreichen ; war aber der am häufigsten eingereichte Begriff ; to send in, to hand in, to submit
endgültig ; Bis zum Ende des Jahrhunderts wird die Hälfte davon endgültig 'außer Betrieb' sein ;finally
vornehmlich ; früher vornehmlich aus Frankreich ; primarily, mainly, expecially
erstaunlich ; Dies ist erstaunlich, weil die Franzosen kaum fremde Wörter annehmen ; surprising, astonishing
besagete ; Das Wort "kaputt" kam bei besagtem Schönheits-Wettbewerb auf Platz 4. ; the said, implied, לעייל
bedenken ; und sei hier mit einer Anekdote bedacht: blah! ; to consider, to keep in mind
einst ; Der Begriff 'nusu kaput' kommt aus Tansania (einst deutsche Kolonie) ; once, formerly
Freude an etwas haben ; Wir können ruhig auch ein wenig stolz darauf sein, dass andere Sprachen unsere Wörter übernehmen und Freude an ihnen haben ; to enjoy something


