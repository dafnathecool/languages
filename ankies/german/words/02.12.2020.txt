grenzwertig ; ; borderline, marginal, edgy
die Überleitung ; ; transition, מנגינת מעבר בתוכניות רדיו

# Sicher test https://www.hueber.de/sicher/oet?niveau=c11

# misslungen ; Heute ist echt nicht mein Tag. Der Kuchen ist total misslungen ; total failure
im Vordergrund stehen ; Wenn ich verreise, steht für mich der Spaß im Vordergrund ; the most important
widmen ; ; to dedicate
# sich verschreiben ; Die hat sich diesem Sport auch vollkommen verschreiben ; to commit to something
der Reiz ; ; stimulus,
# die Eignung ; ; qualification suitability
# genehmigen ; Silke, ich habe gerade meinen Urlaub genehmigt bekommen ; to permit, to approve
die Abrechnung ; ; settlement, final invoice
# der Zuschlag ; ; extra pay, bonus
# das Entgelt ; ; the charge, the paid money
vereinfachen ; das vereinfacht die Übung total ; to simplify
verarbeiten ;  Aufgaben auf einmal verarbeiten ; to process, to handle
der Übergang ; ; transition
einleiten ; ; to initiate, to start, to trigger
erwähnen ; ; to mention


# Easy german podcast
ausreichen ; wie viele Leute gar kein Deutsch lernen, weil Englisch einfach ausreicht. ; to suffice, to last
die Anführungszeichen (pl) ; ich könnte so viele Menschen verstehen — also, in Anführungszeichen verstehen, ; Quotation mark
die Ankündigung ; Ich habe eine Ankündigung ! ; announcement
verdammt ; damit kann man mit verdammt vielen Leuten kommunizieren. ; damned
gestehen ; Ja, ich muss gestehen, ich habe schon angefangen zu gucken. ; to admit

# mit uli nach Hause
das Stroh ; ; straw
der Halm ; ; גבעול
das Getreide ; ; cereal, דגנים
der Weizen ; ; חיטה
der Kranz ; ; זר גלגל
wenden ; ; to turn
aufwendig ; ; elaborate, time consuming
aufwenden ; ; to spend, to expend
wanden ; ; prat of winden - to wind, to twist, etwas drehen
