# 177
süchtig ; Wie kann man denn von Schach süchtig werden? ; addicted
die Abfertigung ; dass das eben nicht nur eine medizinische Abfertigung ist, sondern da sogar eine Psychologin gezeigt wird, die dann kommt. ; handling, serivce
ausklammern ; Ich weiß gar nicht, was wir da groß zu sagen können, aber ich wollte es jetzt auch nicht so ganz ausklammern,  ; to exclude
auserwählen ; Und deswegen habe ich das Projekt "Brille finden" unternommen und habe jetzt eine Brille auserwählt. ; chosen

# 178
ballern ;  in ein Geschäft gehen und dir eine automatische oder halbautomatische Waffe kaufen und damit rumballern. ; shoot
der Amoklauf ; Natürlich gibt es auch in anderen Ländern Amokläufe, aber es ist einfach viel, viel weniger, viel viel weniger tödlich. ; rampage, mass shooting
die Untertreibung ; ich muss das einmal von der Seele bekommen und dir erzählen. Und tatsächlich ist "das nervt" noch eine Untertreibung. ; understatement
die Überschneidung ; Eine offizielle Beschreibung findet ihr auf unserer Website, weil das war jetzt meine Version. Ich hoffe, es gibt da ein bisschen Überschneidung zumindest. ; overlapping
das DÜrre ;  Es gibt nämlich in Taiwan Probleme mit der Dürre und das hängt damit zusammen, Chips herzustellen."  ; drought, בצורת
der Klon ; Also mich sozusagen als Klon. ; clone, שכפול
flapsig ; Ein lustiger Job in einem lustigen Team für Leute … ist das ist das zu flapsig formuliert, Manuel? ; offhand, כלאחר יד
aufgehen ; Und ich sage dann: "Nein, ich gehe das Risiko ein, dass die aufgehen, die Himbeeren. ; to open, to come undone
durchkommen ; denn der Plastik ist leicht, zäh, man kann weder mit Wasser noch mit Luft durchkommen. ; get through
zäh ; denn der Plastik ist leicht, zäh, man kann weder mit Wasser noch mit Luft durchkommen. ; tough

# 179
verfassungswidrig ; Es ist nicht komplett verfassungswidrig, aber es muss nachgebessert werden, ;  unconstitutional
die Hürde ; weil sie noch nicht so gut Deutsch sprechen und auch gar nicht wissen, wo man die herbekommt, und zweitens vielleicht auch, ja, einfach größere Hürden haben. ; hurdle
umlegen ; ich habe so gemerkt, dass ich so, wirklich so einen Schalter in meinem Kopf umgelegt habe, ; turn, shift
niedergeschlagen ; das ist die Assistentin, die dort am Empfang sitzt — und er kam wieder und war total niedergeschlagen ; depressed, כנוע, מוחלש
der Anteil ; und das waren ausgerechnet nicht die Sprachen mit den größten Anteilen von Immigranten in Deutschland. ; share, portion
Hang zu etw. haben ; Und der hat, außerdem hat er einen großen Hang zum Chaos. ; to have tendency to sth.
heraushalten ; tress aus meinem Leben immer weiter herauszuhalten, ; keep sth. outside

# 184

die Zulassungsbehörde ; In dem Moment, wo irgendeine Zulassungsbehörde sagt: "Wir haben einen möglichen Zusammenhang zwischen der BioNTech-Impfung und dem Auftreten von Entzündungen am Herz gefunden. ; licensing authority,  רשות הרישוי
das Vergrößerungsglas ; Und dann schauen wir natürlich mit dem Vergrößerungsglas, mit der Lupe auf dieses eine Problem und vergessen häufig den totalen Kontext davon. ; magnificant glas
ungeheuer ; Formate wie TikTok, die so Unterhaltung für zwischendurch und leichte Kost bieten und auf der Gegenseite aber Formate wie Mai oder Christian Drosten sie machen mit einem ungeheuren Tiefgang,  ; enormous
gelingen ; die man zu einem Thema findet, ich arbeite die so verständlich und nachvollziehbar und linear auf, wie es mir nur gelingt. ; to succeed
der Zorn ; Ich kriege einen Zorn und sage: "Schert euch zum Teufel, wenn ihr nicht offen seid für Argumente." Also, meine Herangehensweise ist, ich nehme die besten wissenschaftlichen Arbeiten, die man zu einem Thema findet, ; anger, wrath, זעם
die Herangehensweise ; Ich kriege einen Zorn und sage: "Schert euch zum Teufel, wenn ihr nicht offen seid für Argumente." Also, meine Herangehensweise ist, ich nehme die besten wissenschaftlichen Arbeiten, die man zu einem Thema findet, ; approach, strategy
wurscht ; Aber wenn jetzt eine seltene Nebenwirkung nur einer von hunderttausend entwickelt, dann ist das wurscht, ob ich das über zwei Monate teste oder über zwanzig Jahre. ; irrelevant, meaningless (slang)
bestechen ; Ich glaube dass alle Leute in der Forschung korrupt sind und bestochen von irgendwelchen Interessensgruppen ; bribe
zurücknehmen ; ich ja viele Leute jetzt über ein, zwei Ecken kenne, die da sehr präsent sind, medial, dass die sich zum Teil schon wieder ein bisschen zurücknehmen aus der Aufklärungsarbeit,   ; לתפוס מרחק (מטאפורית)
gewaltig ; Das heißt, ich habe hier eine gewaltige Latenz, also eine gewaltige Zeit der Verzögerung zwischen dem, was ich von mir gebe und der Reaktion, die ich darauf sehe.  ; enormous, huge, vast, massive
die Latenz ; Das heißt, ich habe hier eine gewaltige Latenz, also eine gewaltige Zeit der Verzögerung zwischen dem, was ich von mir gebe und der Reaktion, die ich darauf sehe.  ; latency
die Verzögerung ; Das heißt, ich habe hier eine gewaltige Latenz, also eine gewaltige Zeit der Verzögerung zwischen dem, was ich von mir gebe und der Reaktion, die ich darauf sehe.  ; delay, time lag
der Laie ; Und ich glaube, da hat man schon einen sehr großen Vorteil, wenn man viele, viele Jahre mit Laien über Forschung gesprochen hat und eigentlich immer sofort gesehen hat an den Gesichtern der Leute, interessiert die das, verstehen, die das? ; layman
entnehmen ; Und bei den Erwachsenen bedeutet das, dann geht das wirklich so weit, dass die dann ihre eigene Erbinformation entnehmen und dort bestimmte Gen-Varianten testen, um zu sehen, wie sie genetisch in Bezug auf diese Eigenschaft zusammengesetzt sind zum Beispiel. ; take from, withdraw, gather
zusammengesetzt ; Und bei den Erwachsenen bedeutet das, dann geht das wirklich so weit, dass die dann ihre eigene Erbinformation entnehmen und dort bestimmte Gen-Varianten testen, um zu sehen, wie sie genetisch in Bezug auf diese Eigenschaft zusammengesetzt sind zum Beispiel. ; composed, compound
zugeschaltet ; Hallo liebe Leute. Wir haben heute endlich wieder einen Gast bei uns im Easy German Podcast und begrüßen heute aus Österreich zugeschaltet den Martin Moder. Hallo Martin! ; connected

# 185
der Nerz ;  Wir haben gesehen, auch Nerzfarmen, wenn das Virus schon mal da ist, ; חורפן, כמו חמוס, mink
hergeben ; Ah, diese gierigen Firmen, die wollen nicht das Patent hergeben und dann wäre alles besser und wir hätten viel mehr Impfstoff." ; give away, hand over
hervorragend ; wir könnten eigentlich diese Pandemie hervorragend kontrollieren ; excellent, brilliant
ausrotten ; Dieses Coronavirus existiert noch, das haben wir nicht ausrotten können, ; eradicate , למגר
entkommen ; Das war auch ein Coronavirus, das wäre uns vor ein paar Jahren fast entkommen. ; to escape from sth.
die Daumenregel ; Das unterscheidet sich von Land zu Land und von Zeitpunkt zu Zeitpunkt, aber so als grobe Daumenregel, wenn ich mit Antikörperuntersuchungen nachschaue, ; role of thumb
die Dunklziffer ; Was, wie hoch ist denn die Dunkelziffer wahrscheinlich? Was schätzt man aktuell?  ; estimated number
der Klacks ; aber im Vergleich zu dem, was da draußen noch ist an Viren, die theoretisch pandemisch werden können, ist das hier nahezu ein Klacks. ; piece of cake
ausgesetzt ;  wenn zum Beispiel meine Eltern in einem Altenheim wären, warum die der Gefahr ausgesetzt sein sollten, dass da irgendwelche Leute vom Personal oder gar große Gruppen sich einfach nicht impfen lassen wollen ; exposed
die Stellung ; Ja, in Wirklichkeit habe ich da keine starke Stellung, weil es halt wirklich sehr darauf ankommt, was die Situation ist,  ; position
ethisch vertreten ; warum man in der Situation von Lehrpersonal, die jeden Tag mit so viel Leuten Kontakt haben, warum man es da ethisch vertreten kann, zu sagen: ; to regard as ethic
abgeschwächt ; dass zumindest bei Kindern der Effekt der Impfung abgeschwächt werden könnte, ; weakened
der Buster ; Das ist jetzt ein Mythos, du bist ja ein Myth Buster. Stimmt das? ; master


# linkge22 Community
die Leinwand ; Bevor es auf den Leinwänden wieder flimmert, sanieren die Passage-Kinos noch ihren Saal „Universum“. ; fabric, canvas
flimmern ; Bevor es auf den Leinwänden wieder flimmert, sanieren die Passage-Kinos noch ihren Saal „Universum“. ; flicker,
der Sperrmüll ; Die kommen raus, sollen jedoch nicht alle auf dem Sperrmüll landen. Deshalb bieten Petra Kleemann und ihr Team 120 gut erhaltene Sitzmöbel zum Verkauf an.  ; bulky waste, פסולת גדולה

# dezentrale
die Aufarbeitung ; festgelegt habe, das für die Rückgewinnung, das Recycling, die Aufarbeitung und die Zerstörung von geregelten Stoffen im Sinne von ; renovation, שיפוץ
die Zwangsstörung ; es ist ok, es ist nur meine Zwangsstörung, meine OCD ; obsessive-compulsive disorder
die Anweisung ; jetzt sollen wir die Anweisung lesen ; instructions
zugreifen ; Er kann auf diese Daten nicht direkt zugreifen ; (computing) to access

# misc
biegsam ; Die Bezeichnung bezieht sich offenbar nicht auf die Hülle, sondern nur auf die biegsame Magnetscheibe im Inneren. ; flexible
die Hülle ; Die Bezeichnung bezieht sich offenbar nicht auf die Hülle, sondern nur auf die biegsame Magnetscheibe im Inneren. ; covering, wrapping, case
überheblich ; das find ich überheblich ; arrogant
