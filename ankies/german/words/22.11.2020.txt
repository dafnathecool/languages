ersticken ; ; suffocate
nachhaltig ; ; lasting, sustained
durchsetzen ; ; to establish, to assert, to enforce
abtreiben ; ; to have an abortion
das Gelände ; ; area, terrain
der Nachlass ; ; inheritance
vorgeben ; ; to pretend
die Schachtel ; ; a small square box
die Fessel ; ; שלשלאות
der Wipfel ;; משרת
unterbringen ; ; to host, to accommodate

