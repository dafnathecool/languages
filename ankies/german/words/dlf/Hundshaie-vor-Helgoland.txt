# https://www.deutschlandfunk.de/erforschung-von-fischrouten-hundshaie-vor-helgoland.676.de.html?dram:article_id=484682
die Sichtung ; Wenn die ersten Meldungen über Sichtungen bei ihm eintreffen ; the sighting
eintreffen ; Wenn die ersten Meldungen über Sichtungen bei ihm eintreffen ; to arrive, to come true
dennoch ; Aber dennoch ist unser Wissen um diese Hai-Art noch sehr lückenhaft ; however
im Endeffekt ;  Dass wir im Endeffekt vor unserer Haustür ´ne Hai-Art haben, von der fast niemand was weiß ; in the end, ultimately
einstufen ; im Februar wurden Hundshaie deshalb als gefährdete Art eingestuft ; classify
der Raubfisch ; müssen die Wissenschaftlerinnen und Wissenschaftler das Verhalten der Raubfische enträtseln… ; predator fish
enträtseln ; müssen die Wissenschaftlerinnen und Wissenschaftler das Verhalten der Raubfische enträtseln… ; to solve , לפצח חידה,
der Köder ; Die Haie werden angelockt mit Ködern und dann ganz klassisch geangelt ; bait, פתיון
nachweisen ; Leider ist es mir noch nicht gelungen, eine Rückwanderung nach Helgoland nachzuweisen. ; to prove
sich tummeln ; Offenbar sind es aber vor allem erwachsene Weibchen, die sich vor Helgoland tummeln ; romp, lebhaft bewegen
vermehrt ; Und die sich vorher vermehrt im holländischen Wattenmeer aufhalten ; increased, augmented
die Kinderstube ; Das Wattenmeer ist die Kinderstube für viele Nordsee-Tiere.  ; nursery
der Nachwuchs ; Also erst für Nachwuchs sorgen und sich dann rund um Helgoland vollfuttern, ; offspring
der Rochen ; in denen Haie und Rochen sich entwickeln können und auch Seepferdchen ; manta ray


