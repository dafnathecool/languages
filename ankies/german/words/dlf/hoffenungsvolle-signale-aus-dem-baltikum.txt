dlf
sich ausbreiten ; Die Afrikanische Schweinepest breitet sich weiter in Europa aus. ; to spread, to spread out
der Fund ; Zehn Kilometer östlich der Neiße, auf polnischem Gebiet, ist der nächste Fund gewesen ; a find
die Neiße ; Zehn Kilometer östlich der Neiße, auf polnischem Gebiet, ist der nächste Fund gewesen ; a river in east Germany
einschleppen ; ist die Angst hier im Landkreis groß, dass das Virus eingeschleppt werden könnte. ; להתגנב
die Sichtweise ; Trotz dieser realistisch-pessimistischen Sichtweise ist er sicher, dass Deutschland momentan noch ASP frei ist. ; view, viewpoint, way of thinking
die Weise ; dass früher oder später doch in der einen oder anderen Weise Deutschland betreffen wird. ; manner, way
die Wiese ; Wir spielen Frisbee auf der Wiese ; meadow, lawn, grass
das Referenzlabor ; Sie leitet das Nationale Referenzlabor für die Afrikanische Schweinepest am Friedrich Loeffler-Institut auf der Ostsee-Insel Riems ; מעבדה למדידיות לרפרנס וקליברציה
der Heil ; Zäune sind keine Heilmittel oder eine Wunderwaffe ; ישועה, גאולה
sich erweisen ; Aber es hat sich eigentlich erwiesen, auch in Belgien und anderen betroffenen Ländern ; to prove, to turn out
der Impfstoff ; Denn obwohl es mit der Impfstoff-Forschung vorangeht, wird es noch ein paar Jahre dauern, bis man ein in der EU einsatzfähiges Präparat haben wird. ; vaccine
einsatzfähig ; Denn obwohl es mit der Impfstoff-Forschung vorangeht, wird es noch ein paar Jahre dauern, bis man ein in der EU einsatzfähiges Präparat haben wird. ; operational
das Präparat ; Denn obwohl es mit der Impfstoff-Forschung vorangeht, wird es noch ein paar Jahre dauern, bis man ein in der EU einsatzfähiges Präparat haben wird. ; preparation
gewillt ; Und dort ist man auch gewillt, Impfstoffe zu akzeptieren, die nicht den europäischen Standards entsprechen ; willing
der Erreger ; Bei Lebendimpfstoffen wird der für die Tiere tödliche Erreger abgeschwächt, indem spezifische Virus-Gene entfernt werden. ;germ, חיידק
abschwächen ; Bei Lebendimpfstoffen wird der für die Tiere tödliche Erreger abgeschwächt, indem spezifische Virus-Gene entfernt werden. ; to weaken, להחליש
anregen ; Ziel ist es, eine Virus-Version herzustellen, die nicht mehr ernsthaft krank macht, den Körper aber trotzdem zu einer Immunantwort anregt. ; to encourage
die Zulassung ; In Europa wäre solch ein Impfstoff nicht zulassungsfähig, hier müssen Impfstoffe in zertifizierten Zelllinien aus dem Labor produziert werden. ; approval
ausrichten ;  Impfstoff ist auf den Einsatz bei Hausschweinen ausgerichtet ; to design to, to orient to, לייעד ל
