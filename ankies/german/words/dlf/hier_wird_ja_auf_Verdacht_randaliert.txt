dlf
feige ; Die Veranstalter verhielten sich feige ; chicken, coward, פחדן
Druck aussetzten ; weil sie sich dem Druck nicht aussetzten ; to be under pressure
das Gerücht ; Der Druck sei meistens virtuell, da gebe es zunächst Gerüchte ; rumour
beruhen ; ist das ein Todesstoß für eine Gesellschaft, die auf Gedanken- und Meinungsfreiheit beruht ; base on
auf Verdacht ; Hier wird ja auf Verdacht randaliert. ; on spec, on suspicion
randalieren ; Hier wird ja auf Verdacht randaliert. ; riot, השתוללות
Druck ausüben ; Diese passten bestimmten Leuten nicht und es werde Druck ausgeübt ; use pressure
der Kulturschaffender ; dass Kulturschaffende, die vom gesprochenen Wort leben, mundtot gemacht werden ; creative artist
mundtot machen ; dass Kulturschaffende, die vom gesprochenen Wort leben, mundtot gemacht werden ; to confine, to restrain
auf etw Bezug nehmen ; Deshalb könne nur auf ihre früheren Wortmeldungen Bezug genommen werden ; to refer to
abkaufen ; Man möchte, dass Leute, die Meinungen haben, die einem nicht passen, der Schneid abgekauft wird ; to bribe, to sell off
der Schneid ; Man möchte, dass Leute, die Meinungen haben, die einem nicht passen, der Schneid abgekauft wird ; courage, daring
veranlassen ; , und man möchte sie zu Büßergesten veranlassen.“ ; to cause
der Wahn ; Man müsse sich fragen, woher dieser „Wahn“ komme, sagt Leggewie. ; mania, craze, illusion, delusion
das Erachtens ; Es handelt sich meines Erachtens um einen Narzissmus der allerkleinsten Differenz. ; opinion (note the gentive form)
der Narzissmus ; Es handelt sich meines Erachtens um einen Narzissmus der allerkleinsten Differenz. ; narcissism
die Bude ; dass ein Veranstalter nicht wolle, dass ihm die Bude demoliert werde. ; booth, kiosk, apartement
demolieren ; dass ein Veranstalter nicht wolle, dass ihm die Bude demoliert werde. ; destroy
das Rückgrat ; Mehr Rückgrat von Kulturveranstaltern, ; spine, backbone
# ; 5:55 Vieles was jetzt passiert, passiert auf der Basis blanker Gerüchte ; 
