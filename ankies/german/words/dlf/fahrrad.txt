dlf
zu Bruch gehen ; das Rennrad meiner Jugend, endgültig zu Bruch gegangen war. ; להתפרק לחלקים, להתנפץ להרבה חלקים
endgültig ; das Rennrad meiner Jugend, endgültig zu Bruch gegangen war. ; finally
überlassen ; Eine Verwandte hat es mir vor Kurzem freundlich überlassen ; להשאיר משהו אצל מישהו
das Zahlenschloss ; Es ist nicht abgesperrt, aber um den Lenker hängt ein Zahlenschloss ; מנעול עם קוד
absperren ; Es ist nicht abgesperrt, aber um den Lenker hängt ein Zahlenschloss ; to shut off
gut in Schuss  ; das Sofa ist noch gut in Schuss ; in good condition
der Parcours ; ein Parcours mit allem, was dazugehört ; דרך חתחתים
das Gedränge ; Schließlich ein großer Platz, wo das Gedränge der gesamten Stadt sich ballt ; crowd
frequentiert ; alles emsig frequentiert von anderen Verkehrsteilnehmern ; busy, busily
gottlob ; Flugzeuge sind unterwegs, mir jedoch gottlob noch nie direkt begegnet. ; thanks good
die Brutstätte ; Leider auch Brutstätte für Spießer-Paranoia.  ; breeding ground, כר פורה )משמ גם מטפוריט(
der Spießer ; Leider auch Brutstätte für Spießer-Paranoia.  ; איש מרובע, קרתן
zu kurz kommen ; Immer komme ich zu kurz! ; miss out, come off badly, לפספס
die Besinnung ; eine kurze therapeutische Besinnung auf Murphys Stadtradfahr-Gesetze ; reflection
verbauen ; Oder verbaut dir wenigstens mit einer unvermittelt ausgestreckten Tür‑Pranke den Weg ; block, obstruct
unvermittelt ; Oder verbaut dir wenigstens mit einer unvermittelt ausgestreckten Tür‑Pranke den Weg ; unexpected
ausgestreckten ; Oder verbaut dir wenigstens mit einer unvermittelt ausgestreckten Tür‑Pranke den Weg ; stretched out
der Schlamassel ; Jedes einmal eingetretene Schlamassel steigert die Wahrscheinlichkeit für weitere ; mess
eintreten ; Jedes einmal eingetretene Schlamassel steigert die Wahrscheinlichkeit für weitere ; to enter, to join, to occur
flanieren ; die Natur oder sich selbst erfahren, flanieren, meditieren, ; to stroll
anzetteln ; Planeten retten oder eine Revolution anzetteln ; start something negative (a war) (umg)
sagenhaft ; ; legendary, mythical
der Dschungel ; cooler Desperado im Asphaltdschungel ; jungle
ungeschoren ;  der halbwegs schnell und ungeschoren von A nach B möchte ; פתור ממס
ankommen ; Aber darauf kommt es unter den gegebenen Verhältnissen auch gar nicht an ; to be important, to matter
