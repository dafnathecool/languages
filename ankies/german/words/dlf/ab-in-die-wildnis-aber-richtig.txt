dlf
die Wacht ; Sie ist Rangerin bei Naturwacht Brandenburg ; guard
die Einstellung ; Allerdings habe nicht jeder eine naturschützende Einstellung ; attitude
alles über einen Kamm scheren ; Ich möchte nicht alles über einen Kamm scheren. Es gibt viele Leute die sehr verantwortungsbewusst ; להכליל, To characterize using the same undesirable attribute, especially unjustly.
der Prozentsatz ; Es gibt sehr großer Prozentsatz der Müll hinterließt ; percentage
der Ansatz ; unserer Ansatz ist auch stark dass wir wirklich viel Bildungsarbeit betreibe um die Verhaltensregeln in der Natur zu fördern. ; approach
predigen ; dass man dieses Wissen ab dem Kindergartenalter einfach immer wieder predigt ; to preach, להטיף
die Störung ; wo geschützte Arten leben, die sehr störungsempfindlich sind. ; disturbance
der Schwarzstorch ; Beispiel Schwarzstorch: der hat eine Fluchtdistanz von drei- bis fünfhundert Metern und verlässt im schlimmsten Falle seine Brut, wenn er verschreckt wird ; black stork, חסידה שחורה
verlassen ; Beispiel Schwarzstorch: der hat eine Fluchtdistanz von drei- bis fünfhundert Metern und verlässt im schlimmsten Falle seine Brut, wenn er verschreckt wird ; to leave, to abandon
die Brut ; Beispiel Schwarzstorch: der hat eine Fluchtdistanz von drei- bis fünfhundert Metern und verlässt im schlimmsten Falle seine Brut, wenn er verschreckt wird ; offspring, צאצא
verschreckt ; Beispiel Schwarzstorch: der hat eine Fluchtdistanz von drei- bis fünfhundert Metern und verlässt im schlimmsten Falle seine Brut, wenn er verschreckt wird ; scared
# 7:00
# https://www.wildes-sh.de/ Nationalpark Hunsrück
# https://www.nationalpark-hunsrueck-hochwald.de/besucher/essen-trinken-uebernachten/trekking-camps.html
# https://1nitetent.com/en/home/
# https://campspace.com/en/
die Streuobstwiese ; kann man jede Zeit auch freundlich beim nächsten Landwirt anfragen werden vielleicht ein Streuobstwiese hat oder ein nicht bestelltes Feld ; בוסתן
ein bestelltes Feld ; kann man jede Zeit auch freundlich beim nächsten Landwirt anfragen werden vielleicht ein Streuobstwiese hat oder ein nicht bestelltes Feld ; שדה זרוע
der Landwirt ; kann man jede Zeit auch freundlich beim nächsten Landwirt anfragen werden vielleicht ein Streuobstwiese hat ; farmer

offen gegenüberstehen ; sie offen gegenüberstehen das man dort auch mal eine Nacht verbringt. ; to be open to
nach wie vor ; Naturschutzgebieten, ich wurde nach wie vor sagen, Nein ; still
ausrollen ; Ich habe meine Isomatte ja ausgerollt ; to roll out
vertreten ; ist das denn okey aus deiner Sicht wenn du jetzt die Natur vertrittst ; to represent
# 9:50
die Plane ; die einige erlauben Übernachten ohne Zelt, die andere mit ein Tarp also so eine Plane ; tarp,  יריעת פלסטיק, ברזנט
vor Ort ; muss man sich tatsächlich immer vor Ort genau erkundigen ; in the place, במקום
erkundigen ; muss man sich tatsächlich immer vor Ort genau erkundigen ; to inquire
die Perl ; Sie empfiehlt für den Sommerurlaub 2020 die Großschutzgebiete in Deutschland, die sie die "Perlen" von ganz Deutschland nennt. ; pearl, פנינה
das Ballungsgebiet ; Großschutzgebiete sind da wo es sehr sehr ländlich ist, außerhalb der Ballungsgebiete ; metropolin
