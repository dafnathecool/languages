Hi,
This is my ankies repo, it is used for vocabulary of German and English.

TL:DR
-----
To skip the Anki horrible APP and use a lightweight fun command line version:

For German:
```
cd german

# prepare the list of words with their context:
./prepare-anki.py -o german-words.txt

# choose random 30 words from the list:
./myanki.py -r -n 30 -i german-words.txt
```
Now you get something that looks like that:
```
== 1 ==
am Hebel sitzen
Und es wird sich zeigen, wer dann am längeren Hebel sitzt

```
The first line is the word/phrase to learn.
The second line is an example of a sentence with the word/phrase.
Then it waits to your input. And then it shows the English/Hebrew translation and move to then next word.

So you get a nice random list of words each time. No tedious lists and complicated nightmare configurable Anki algorithm.


The directory `german`:
----------------------
Each txt file has a list of ankies, each line has 3 fields separated by ';'.
The first field is the German word, the second is a sentence in which I found the word in
and the 3rd field is the English or sometimes Hebrew translation, or rarely a German synonym.
the python script `prepare-ankies.py` iterates all txt files in the directory and dump them
to one txt file and warns about issues found.
When importing the file to Anki, You probably want to prepare a note type that has those 3 fields.
I use the following card type for example:
```
{{Front}}
<br>
<br>
<div style='font-family: Liberation Sans; font-size: 20px;'>{{Context}}</div>
<br>
{{type:Back}}
<br>
<br>
<p style="font-size: 16px;">{{Tags}}</p>
```
Note the field `Context` which is the example sentence - the second field in the txt files.

To find spelling mistakes in German and English I run:
```
cat myfile.txt | aspell -l en_US -d en  list  | aspell -l de_DE  list | sort | uniq
```

Checking for spelling in both German and English:
```
aspell -l en_US -d en  list  | aspell -l de_DE  list | sort | uniq | less
```

The directory `english`
------------------------
Contains one file `English.txt` which has the same formats as the txt files in the `german`
directory.


### Cram mode in Anki
Since Anki with repetition is extremely boring, I prefer to use it in 'cram' mode
were there is no reschudeling (words don't repeat).
To do this:
Click on the deck you want to cram -> click "Custom Study" at the bottom of the window ->
Choose "Study by card state or tag" -> Choose "All cards in random order(cram mode)".
(Then a pop up window will appear, just close it)


### german-ankies

```
# All what don't contain exactly one semicolon
grep -rv "[^;]*;[^;]*" | grep -v "#"
# all that contain more than one '-'
grep -r " - .* - "

```
This command will show you the words that appear more than once:
```
grep -oi  "^[^;]*;"  . -rh | sort | uniq -d
```

Writing exercise - things to consider
======================================

1. the place of the verb in the sentence
2. watch the cases !
3. does the infinitive verb need a ‘zu’?
4. the preposition which the verb come with.
5. Nouns in capital letters.
6. non plural nouns will almost always need an article.
7. remember the umlauts
8. placing comma signs: a comma sign should always separate subsentences (Nebensatz from Hauptsatz)
9. check spelling!
10. check how words should be declined to plural, plura+dativ


List of similar words
=====================
berühren = to touch
beruhen = based on

die Wende = turn, turnaround
die Wand = wall

der Fluch = the curse
die Flucht = escape, flight

